# Ruby on Rails Tutorial: sample application

This is the sample application that I created while following the 
[*Ruby on Rails Tutorial*](http://www.railstutorial.org/)
book by [Michael Hartl](http://www.michaelhartl.com/).